import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Layout from './Layout';
import NewTask from '../scenes/NewTask';
import TaskList from '../scenes/TaskList';
import Main from '../scenes/Main';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Layout>
          <Switch>
            <Route path="/new-task" component={NewTask} />
            <Route path="/task-list" component={TaskList} />
            <Route path="/" component={Main} />
          </Switch>
        </Layout>
      </React.Fragment>
    );
  }
}

export default App;
