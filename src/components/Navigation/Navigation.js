import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import * as actions from '../../store/actions';

import NavItem from './components/NavItem';
import Logo from './components/Logo';

const navigation = props => {
  const { classes, onResetHandler } = props;
  return (
    <nav className={classes.root}>
      <Logo />
      <div className={classes.navList}>
        <NavItem link="/">Menu</NavItem>
        <NavItem link="/new-task" clicked={onResetHandler}>
          New Task
        </NavItem>
        <NavItem link="/task-list">Task List</NavItem>
      </div>
    </nav>
  );
};

const styles = {
  root: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '1.5rem 2rem',
    boxShadow: '0 1px 4px #ccc',
  },
  navList: {
    textAlign: 'right',
  },
};

navigation.propTypes = {
  classes: PropTypes.object,
  onResetHandler: PropTypes.func,
};

const mapDispatchToProps = dispatch => ({
  onResetHandler: () => dispatch(actions.resetSubmitted()),
});

export default connect(
  null,
  mapDispatchToProps
)(withStyles(styles)(navigation));
