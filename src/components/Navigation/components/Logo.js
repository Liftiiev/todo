import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core';

const logo = props => {
  const { classes } = props;

  return (
    <Link to="/" className={classes.root}>
      <span className={classes.green}>TODO</span>
      .JS
    </Link>
  );
};

const styles = {
  root: {
    fontSize: '2.5rem',
    fontWeight: '700',
    color: '#1A86FB',
    textDecoration: 'none',
  },
  green: {
    color: '#4CAF50',
  },
};

logo.propTypes = {
  classes: PropTypes.object,
};

export default withStyles(styles)(logo);
