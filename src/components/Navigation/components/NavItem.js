import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';

const navItem = props => {
  const { classes, link, clicked, children } = props;

  return (
    <NavLink
      to={link}
      exact
      activeClassName={classes.active}
      className={classes.root}
      onClick={clicked}
    >
      {children}
    </NavLink>
  );
};

const styles = {
  root: {
    padding: '.1rem',
    marginLeft: '2rem',
    color: '#2d3436',
    fontSize: '1.2rem',
    textTransform: 'uppercase',
    textDecoration: 'none',
  },
  active: {
    borderBottom: '3px solid #1A86FB',
  },
};

navItem.propTypes = {
  link: PropTypes.string,
  clicked: PropTypes.func,
  classes: PropTypes.object,
  children: PropTypes.string,
};

export default withStyles(styles)(navItem);
