import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';

const propTypes = {
  classes: PropTypes.object,
  valid: PropTypes.bool,
  changed: PropTypes.func,
  value: PropTypes.string,
};

const input = props => {
  const { classes, valid, changed, value } = props;
  const inputClass = [classes.input];

  if (valid) {
    inputClass.push(classes.valid);
  }

  return (
    <label htmlFor="taskName" className={classes.inputLabel}>
      Task Name
      <input
        id="taskName"
        onChange={changed}
        value={value}
        className={inputClass.join(' ')}
        type="text"
        placeholder="please enter task name ..."
      />
    </label>
  );
};

const styles = {
  inputLabel: {
    display: 'block',
    width: '80%',
    marginBottom: '1rem',
    fontSize: '1.5rem',
    textTransform: 'uppercase',
    color: '#2d3436',
  },
  input: {
    display: 'block',
    width: '100%',
    padding: '.5rem 0',
    margin: '0 auto',
    border: '0',
    borderBottom: '2px solid #2d3436',
    fontSize: '1.5rem',
    color: '#2d3436',
    background: 'transparent',
    outline: 'none',
    resize: 'none',
    '&:focus': {
      borderBottom: '2px solid #1A86FB',
    },
    '&::placeholder': {
      textTransform: 'uppercase',
      fontSize: '1rem',
      color: '#ccc',
    },
  },
  valid: {
    borderBottom: '2px solid #4cd137',
  },
};

input.propTypes = propTypes;

export default withStyles(styles)(input);
