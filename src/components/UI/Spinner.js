import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const propTypes = {
  classes: PropTypes.object,
};

const spinner = ({ classes }) => <div className={classes.loader} />;

const styles = {
  loader: {
    position: 'absolute',
    top: '50%',
    left: '45%',
    transform: 'translate(-50%, -50%)',
    width: '6rem',
    height: '6rem',
    border: '10px solid #1A86FB',
    borderLeft: '10px solid transparent',
    borderRight: '10px solid transparent',
    borderRadius: '50%',
    animation: 'circle 1s infinite ease-in-out',
  },
  '@keyframes circle': {
    '0%': {
      transform: 'rotate(0deg)',
    },
    '100%': {
      transform: 'rotate(360deg)',
    },
  },
};

spinner.propTypes = propTypes;

export default withStyles(styles)(spinner);
