import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';

const propTypes = {
  classes: PropTypes.object,
  buttonTypeClass: PropTypes.string,
  clicked: PropTypes.func,
  children: PropTypes.string,
};

const button = props => {
  const { classes, buttonTypeClass, clicked, children } = props;
  const btnClass = [classes.root];

  if (buttonTypeClass) {
    btnClass.push(classes[buttonTypeClass]);
  }

  return (
    <button className={btnClass.join(' ')} onClick={clicked} type="submit">
      {children}
    </button>
  );
};

const styles = {
  root: {
    display: 'block',
    width: '200px',
    margin: '.5rem auto',
    padding: '.5rem',
    borderRadius: '5px',
    textTransform: 'uppercase',
    fontSize: '1.2rem',
    background: '#0984e3',
    color: '#fff',
    cursor: 'pointer',
    outline: 'none',
    transition: '.3s ease-in-out',
    '&:hover': {
      background: '#1A86FB',
    },
    '&:disabled': {
      background: '#ccc',
    },
  },
  success: {
    padding: '.5rem 2rem',
    background: '#4CAF50',
    '&:hover': {
      background: '#108a44',
    },
  },
  delete: {
    padding: '.5rem 2rem',
    background: '#e74c3c',
    '&:hover': {
      background: '#af1303',
    },
  },
};

button.propTypes = propTypes;

export default withStyles(styles)(button);
