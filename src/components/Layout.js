import React from 'react';
import PropTypes from 'prop-types';

import Navigation from './Navigation';

const propTypes = {
  children: PropTypes.object,
};

const layout = ({ children }) => (
  <React.Fragment>
    <Navigation />
    {children}
  </React.Fragment>
);
layout.propTypes = propTypes;

export default layout;
