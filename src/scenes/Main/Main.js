import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import * as actions from '../../store/actions';

const propTypes = {
  classes: PropTypes.object,
  onResetHandler: PropTypes.func,
};

const main = props => {
  const { classes, onResetHandler } = props;
  return (
    <div className={classes.root}>
      <Link to="/new-task" className={classes.link} onClick={onResetHandler}>
        New Task
      </Link>
      <Link to="/task-list" className={classes.link}>
        Task List
      </Link>
    </div>
  );
};

const styles = {
  root: {
    width: '100%',
    margin: '10rem auto',
  },
  link: {
    display: 'block',
    width: '200px',
    padding: '1rem 2rem',
    margin: '1rem auto',
    borderRadius: '8px',
    textAlign: 'center',
    textTransform: 'uppercase',
    textDecoration: 'none',
    fontSize: '1.5rem',
    background: '#0984e3',
    color: 'white',
    transition: 'all .3s ease-in',
    '&:hover': {
      background: '#0068b9',
    },
  },
};

main.propTypes = propTypes;

const mapDispatchToProps = dispatch => ({
  onResetHandler: () => dispatch(actions.resetSubmitted()),
});

export default connect(
  null,
  mapDispatchToProps
)(withStyles(styles)(main));
