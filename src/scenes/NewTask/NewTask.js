import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import Form from './components/Form';

const addTask = props => {
  let addNewTask = <Redirect to="/task-list" />;

  if (!props.submitted) {
    addNewTask = (
      <div>
        <h3 className={props.classes.title}>Add Task</h3>
        <Form />
      </div>
    );
  }

  return addNewTask;
};

const styles = {
  title: {
    textAlign: 'center',
    fontSize: '2.5rem',
    textTransform: 'uppercase',
    color: '#1a86fb',
    marginBottom: '0',
  },
};

const mapStateToProps = state => ({
  submitted: state.submitted,
});

export default connect(mapStateToProps)(withStyles(styles)(addTask));
