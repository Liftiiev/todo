import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import Calendar from 'react-calendar';
import * as actions from '../../../store/actions';
import Input from '../../../components/UI/Input';
import Button from '../../../components/UI/Button';
import Spinner from '../../../components/UI/Spinner';

const propTypes = {
  classes: PropTypes.object,
  loading: PropTypes.bool,
  newTaskHandler: PropTypes.func,
};

const inititalState = {
  title: '',
  dueDate: null,
  completed: false,
  titleError: '',
  dateError: '',
  submitted: false,
};

class Form extends Component {
  state = inititalState;

  onDateHandler = date => {
    this.setState({
      dueDate: date,
    });
  };

  onTitleHandler = e => {
    this.setState({
      title: e.target.value,
    });
  };

  formValidation = () => {
    const { title, dueDate } = this.state;
    let titleError = '';
    let dateError = '';

    if (!title) {
      titleError = 'Name is empty!';
    }

    if (!dueDate) {
      dateError = 'Сhoose a date!';
    }

    if (dateError || titleError) {
      this.setState({ dateError, titleError });
      return false;
    }

    return true;
  };

  onSubmitHandler = e => {
    e.preventDefault();
    const { title, dueDate } = this.state;
    const { newTaskHandler } = this.props;
    const valid = this.formValidation();

    const data = {
      title,
      dueDate,
      completed: false,
    };

    if (valid) {
      newTaskHandler(data);
      this.setState(inititalState);
    }
  };

  render() {
    const { title, titleError, dateError, isValid } = this.state;
    const { classes, loading } = this.props;

    let form = (
      <form className={classes.form} onSubmit={this.onSubmitHandler}>
        <Input changed={this.onTitleHandler} value={title} valid={isValid} />
        <div className={classes.error}>{titleError}</div>
        <Calendar className={classes.calendar} onChange={this.onDateHandler} />
        <div className={classes.error}>{dateError}</div>
        <Button>Add Task</Button>
      </form>
    );

    if (loading) {
      form = <Spinner />;
    }

    return <div className={classes.root}>{form}</div>;
  }
}

const styles = {
  root: {
    padding: '3rem 0',
  },
  title: {
    display: 'inline-block',
    margin: '0 0 3rem',
    textTransform: 'uppercase',
    borderBottom: '1px solid #388E3C',
    fontSize: '2rem',
    color: '#BDBDBD',
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '70%',
    margin: '0 auto',
    padding: '3rem 0',
    background: '#fff',
    boxShadow: '2px 2px 4px #ccc',
  },
  calendar: {
    padding: '30px',
    marginBottom: '1rem',
    border: 'none',
    boxShadow: '2px 2px 2px #ccc',
  },
  error: {
    dispaly: 'block',
    height: '2rem',
    color: 'red',
  },
};

Form.propTypes = propTypes;

const mapStateToProps = state => ({
  loading: state.loading,
});

const mapDispatchToProps = dispatch => ({
  newTaskHandler: data => dispatch(actions.newTaskInit(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(withStyles(styles)(Form)));
