import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import * as actions from '../../store/actions';

import Task from './components/Task';
import Modal from './components/Modal';
import Spinner from '../../components/UI/Spinner';

const propTypes = {
  classes: PropTypes.object,
  listInitHandler: PropTypes.func,
  showModalHandler: PropTypes.func,
  listDeleteHandler: PropTypes.func,
  loading: PropTypes.bool,
  tasks: PropTypes.array,
  saving: PropTypes.bool,
  editing: PropTypes.bool,
};

class TaskList extends Component {
  componentDidMount() {
    const { listInitHandler } = this.props;
    listInitHandler();
  }

  dateHandler = date => {
    const newDate = date.slice(0, 10);
    return newDate;
  };

  checkDate = date => {
    const currentDate = new Date();
    const taskDate = new Date(date);

    if (currentDate >= taskDate) {
      return false;
    }

    return true;
  };

  render() {
    const {
      classes,
      loading,
      tasks,
      saving,
      editing,
      listDeleteHandler,
      showModalHandler,
    } = this.props;

    let taskList = <Spinner />;
    let emptyList = null;

    if (!loading && tasks <= 0) {
      emptyList = <h3 className={classes.empty}>Your Task list is Empty!</h3>;
    }

    if (!loading && tasks) {
      taskList = tasks.map((el, i) => (
        <Task
          key={i}
          completed={el.completed}
          missed={this.checkDate(el.dueDate)}
          taskNum={i}
          title={el.title}
          date={this.dateHandler(el.dueDate)}
          clicked={() => listDeleteHandler(el._id)}
          editing={() => showModalHandler(el)}
        />
      ));
    }

    return (
      <div>
        {saving ? <Spinner /> : null}
        <h3 className={classes.title}>TASK LIST</h3>
        <ul className={classes.list}>
          {emptyList}
          {taskList}
        </ul>
        {editing ? <Modal /> : null}
      </div>
    );
  }
}

const styles = {
  title: {
    textAlign: 'center',
    fontSize: '2.5rem',
    textTransform: 'uppercase',
    color: '#1a86fb',
    marginBottom: '0',
  },
  list: {
    margin: '2rem 0 0',
    padding: '0',
  },
  empty: {
    textAlign: 'center',
    fontSize: '2rem',
    color: '#ccc',
    margin: '10rem auto',
  },
};

TaskList.propTypes = propTypes;

const mapStateToProps = state => ({
  tasks: state.tasks,
  loading: state.loading,
  editing: state.isEditing,
});

const mapDispatchToProps = dispatch => ({
  listInitHandler: () => dispatch(actions.taskListInit()),
  listDeleteHandler: id => dispatch(actions.taskListDelete(id)),
  showModalHandler: el => dispatch(actions.taskShowModal(el)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(TaskList));
