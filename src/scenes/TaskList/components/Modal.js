import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import Checkbox from '@material-ui/core/Checkbox';
import Calendar from 'react-calendar';
import * as actions from '../../../store/actions';

import Spinner from '../../../components/UI/Spinner';
import Input from '../../../components/UI/Input';
import Button from '../../../components/UI/Button';

const propTypes = {
  classes: PropTypes.object,
  loading: PropTypes.bool,
  taskHideModal: PropTypes.func,
  taskSaveChanges: PropTypes.func,
  editedElem: PropTypes.object,
};

class Modal extends Component {
  state = {
    title: '',
    completed: false,
    dateError: '',
    titleError: '',
  };

  componentDidMount() {
    const { editedElem } = this.props;
    this.setState({
      dueDate: editedElem.dueDate,
      id: editedElem._id,
      title: editedElem.title,
      completed: editedElem.completed,
    });
  }

  onCheckboxHandler = e => {
    const { completed } = this.state;
    this.setState({
      completed: !completed,
    });
  };

  onDateHandler = date => {
    this.setState({
      dueDate: date,
    });
  };

  onChangeHandler = e => {
    this.setState({
      title: e.target.value,
    });
  };

  formValidation = () => {
    const { title, dueDate } = this.state;
    let titleError = '';
    let dateError = '';

    if (!title) {
      titleError = 'Name is empty!';
    }

    if (!dueDate) {
      dateError = 'Сhoose a date!';
    }

    if (dateError || titleError) {
      this.setState({ dateError, titleError });
      return false;
    }

    return true;
  };

  onSubmitHandler = e => {
    e.preventDefault();
    const valid = this.formValidation();
    const { title, dueDate, completed, id } = this.state;
    const { taskSaveChanges } = this.props;

    const data = {
      title,
      dueDate,
      completed,
      id,
    };

    if (valid) {
      taskSaveChanges(data);
    }
  };

  render() {
    const { classes, loading, taskHideModal } = this.props;
    const { titleError, dateError, title, completed } = this.state;

    console.log(completed);

    let modal = <Spinner className={classes.spinner} />;

    if (!loading) {
      modal = (
        <form className={classes.editModal} onSubmit={this.onSubmitHandler}>
          <Input value={title} changed={this.onChangeHandler} />
          <div className={classes.error}>{titleError}</div>
          <Calendar
            className={classes.calendar}
            onChange={this.onDateHandler}
          />
          <div className={classes.error}>{dateError}</div>
          <div className={classes.checkboxWrapper}>
            <Checkbox
              className={classes.checkbox}
              color="default"
              onChange={this.onCheckboxHandler}
              checked={completed}
            />
            <span className={classes.label}>Mark as Done</span>
          </div>
          <Button class="primary">Save changes</Button>
        </form>
      );
    }

    return (
      <React.Fragment>
        <div
          role="button"
          className={classes.editWrapper}
          onClick={taskHideModal}
          onKeyDown={() => {}}
          tabIndex="0"
        />
        {modal}
      </React.Fragment>
    );
  }
}

const styles = {
  editWrapper: {
    position: 'fixed',
    background: 'rgba(0, 0, 0, .8)',
    top: '0',
    left: '0',
    width: '100%',
    height: '100%',
  },
  editModal: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '70%',
    margin: '0 auto',
    padding: '3rem 0',
    background: '#fff',
    boxShadow: '2px 2px 4px #333',
  },
  calendar: {
    padding: '30px',
    marginBottom: '1rem',
    border: 'none',
    boxShadow: '2px 2px 2px #ccc',
  },
  checkboxWrapper: {
    display: 'flex',
    alignItems: 'center',
  },
  checkbox: {
    color: 'green',
    '&:checked': {
      color: 'green',
      background: 'green',
    },
  },
  label: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    margin: '1rem 0',
    fontSize: '1.4rem',
  },
  error: {
    dispaly: 'block',
    height: '2rem',
    color: 'red',
  },
  spinner: {
    border: '3px solid red',
    position: 'absolute',
    top: '0',
  },
};

Modal.propTypes = propTypes;

const mapStateToProps = state => ({
  editedElem: state.editedElem,
  loading: state.loading,
});

const mapDispatchToProps = dispatch => ({
  taskHideModal: () => dispatch(actions.taskHideModal()),
  taskSaveChanges: data => dispatch(actions.taskSaveChanges(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Modal));
