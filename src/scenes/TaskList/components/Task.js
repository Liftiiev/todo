import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import Button from '../../../components/UI/Button';

const propTypes = {
  classes: PropTypes.object,
  completed: PropTypes.bool,
  missed: PropTypes.bool,
  title: PropTypes.string,
  date: PropTypes.string,
  clicked: PropTypes.func,
  editing: PropTypes.func,
};

const listItem = props => {
  const { classes, completed, missed, title, date, clicked, editing } = props;
  const dateClass = [classes.date];
  const nameClass = [classes.name];

  if (completed) {
    nameClass.push(classes.isDone);
  }

  if (!missed) {
    dateClass.push(classes.missed);
  }

  return (
    <li className={classes.task}>
      <div className={classes.content}>
        <h4 className={nameClass.join(' ')}>{title}</h4>
        <span className={dateClass.join(' ')}>{date}</span>
      </div>
      <div>
        <Button class="delete" clicked={clicked}>
          delete
        </Button>
        <Button class="success" clicked={editing}>
          view
        </Button>
      </div>
    </li>
  );
};

const styles = {
  task: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '60%',
    padding: '30px',
    margin: '1rem auto',
    border: 'none',
    boxShadow: '2px 2px 4px #ccc',
  },
  content: {
    dispaly: 'flex',
    flexDirection: 'column',
    alignItems: 'space-between',
    padding: '0 2rem',
  },
  name: {
    fontSize: '1.5rem',
    fontWeight: '300',
    margin: '0 0 1rem',
  },
  date: {
    fontWeight: 700,
    color: '#388E3C',
  },
  missed: {
    color: '#e74c3c',
  },
  isDone: {
    textDecoration: 'line-through',
  },
};

listItem.propTypes = propTypes;

export default withRouter(withStyles(styles)(listItem));
