import * as actionTypes from '../actions/actionTypes';

const initialState = {
  loading: false,
  submitted: false,
  tasks: [],
  editedElem: null,
  isEditing: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    // LOADING
    case actionTypes.LOADING_START:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.LOADING_SUCCESS:
      return {
        ...state,
        loading: false,
      };
    case actionTypes.LOADING_FAIL:
      return {
        ...state,
        loading: false,
      };
    case actionTypes.RESET_SUBMITTED:
      return {
        ...state,
        submitted: false,
      };
    // NEW TASK
    case actionTypes.NEW_TASK_SUBMITTED:
      return {
        ...state,
        submitted: true,
      };
    // TASK LIST
    case actionTypes.TASK_LIST_SUCCESS:
      return {
        ...state,
        tasks: action.taskList,
      };
    case actionTypes.TASK_LIST_FAIL:
      return {
        ...state,
        tasks: [],
      };
    // TASK EDIT
    case actionTypes.TASK_SHOW_MODAL:
      return {
        ...state,
        isEditing: true,
        editedElem: action.editedElem,
      };
    case actionTypes.TASK_HIDE_MODAL:
      return {
        ...state,
        isEditing: false,
      };
    default:
      return state;
  }
};

export default reducer;

// case actionTypes.ADD_TASK_SUCCESS:
//       return {
//         ...state,
//         loading: false,
//         submitted: true
//       };
