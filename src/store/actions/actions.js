import * as actionTypes from './actionTypes';
import axios from '../../axios-orders';

// LOADING ACTIONS
export const loadingStart = () => ({
  type: actionTypes.LOADING_START,
});

export const loadingFail = () => ({
  type: actionTypes.LOADING_FAIL,
});

export const loadingSuccess = () => ({
  type: actionTypes.LOADING_SUCCESS,
});

export const resetSubmitted = () => ({
  type: actionTypes.RESET_SUBMITTED,
});

// NEW TASK ACTIONS
export const newTaskSubmitted = () => ({
  type: actionTypes.NEW_TASK_SUBMITTED,
});

export const newTaskInit = data => dispatch => {
  dispatch(loadingStart());
  axios
    .post('/todos', data)
    .then(response => {
      dispatch(loadingSuccess());
      dispatch(newTaskSubmitted());
    })
    .catch(error => {
      dispatch(loadingFail());
    });
};

// TASK LIST ACTIONS
export const taskListSuccess = tasks => ({
  type: actionTypes.TASK_LIST_SUCCESS,
  taskList: tasks,
});

export const taskListFail = () => ({
  type: actionTypes.TASK_LIST_SUCCESS,
});

export const taskListInit = () => dispatch => {
  dispatch(loadingStart());
  axios
    .get('/todos')
    .then(response => {
      const tasks = Object.keys(response.data).map((el, i) => ({
        ...response.data[el],
      }));
      dispatch(taskListSuccess(tasks));
      dispatch(loadingSuccess());
    })
    .catch(error => {
      dispatch(taskListFail());
      dispatch(loadingFail());
    });
};

export const taskListDelete = id => dispatch => {
  dispatch(loadingStart());
  axios
    .delete(`/todos/${id}`)
    .then(response => {
      dispatch(taskListInit());
      dispatch(loadingSuccess());
    })
    .catch(error => {
      dispatch(loadingFail());
    });
};

// TASK EDIT
export const taskHideModal = () => ({
  type: actionTypes.TASK_HIDE_MODAL,
});

export const taskShowModal = el => ({
  type: actionTypes.TASK_SHOW_MODAL,
  editedElem: el,
});

export const taskSaveChanges = data => dispatch => {
  dispatch(loadingStart());
  axios
    .patch(`/todos/${data.id}`, data)
    .then(response => {
      dispatch(taskListInit());
      dispatch(taskHideModal());
      dispatch(loadingSuccess());
    })
    .catch(error => {
      dispatch(taskListFail());
    });
};
